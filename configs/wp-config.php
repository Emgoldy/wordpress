<?php
//Begin Really Simple SSL Load balancing fix
if ((isset($_ENV["HTTPS"]) && ("on" == $_ENV["HTTPS"]))
|| (isset($_SERVER["HTTP_X_FORWARDED_SSL"]) && (strpos($_SERVER["HTTP_X_FORWARDED_SSL"], "1") !== false))
|| (isset($_SERVER["HTTP_X_FORWARDED_SSL"]) && (strpos($_SERVER["HTTP_X_FORWARDED_SSL"], "on") !== false))
|| (isset($_SERVER["HTTP_CF_VISITOR"]) && (strpos($_SERVER["HTTP_CF_VISITOR"], "https") !== false))
|| (isset($_SERVER["HTTP_CLOUDFRONT_FORWARDED_PROTO"]) && (strpos($_SERVER["HTTP_CLOUDFRONT_FORWARDED_PROTO"], "https") !== false))
|| (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && (strpos($_SERVER["HTTP_X_FORWARDED_PROTO"], "https") !== false))
|| (isset($_SERVER["HTTP_X_PROTO"]) && (strpos($_SERVER["HTTP_X_PROTO"], "SSL") !== false))
) {
$_SERVER["HTTPS"] = "on";
}
//END Really Simple SSL

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv("MYSQL_DATABASE") );

/** MySQL database username */
define( 'DB_USER', getenv("MYSQL_USER") );

/** MySQL database password */
define( 'DB_PASSWORD', getenv("MYSQL_PASSWORD") );

/** MySQL hostname */
define( 'DB_HOST', getenv("MYSQL_HOST") );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '7rG6wFM7bwPzLXm9o6r7ZXYu5cFtGNFU4NHN7I4YDs5SlRka6c' );
define( 'SECURE_AUTH_KEY', 'TRARnQ7pAJsRo31T14hF0e2pFvp33YUluaBcerdiOwdOQlHLYn' );
define( 'LOGGED_IN_KEY', 'MtcKtx8FyMNz9jzsDCSdlZhtdlCYAYDEGPuaaabE8nNvv8bIEw' );
define( 'NONCE_KEY', 'dseYhCJRaj7lMdUybJJtqNYCE6ZAgxublnWGYrXv2qoGlNY1ci' );
define( 'AUTH_SALT', 'f3VzpvJUbmyWQbfNFC1mKjdvpO03Ar5YW64yk8lQ6P4I17NlJx' );
define( 'SECURE_AUTH_SALT', '5Sh8FxYHdDmsOejMDpMbBKMO4Y7CV8s2zaWlLKLXNkfHmRJ0xp' );
define( 'LOGGED_IN_SALT', '5csESILy1NYZjDV7i5oqqrkYbKdRWETI6jJhWcPwZdHZ9YGS31' );
define( 'NONCE_SALT', 'QyZl0IKH7wo94uBp5zd3ejdNoXiH6dyVWPLUd4f1SSqoKAF0gx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = getenv("MYSQL_TABLE_PREFIX");

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
